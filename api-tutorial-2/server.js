const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const PORT = process.env.PORT || 8080;
const app = express();
const mongoose = require('./db/mongoose');
const { Note } = require('./db/note.model')


let corsOptions = {
  origin: 'localhost:8081'
};

app.use(cors(corsOptions));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json())

// **************************************************************************************************************************

app.get('/notes', (req, res) => {
  Note.find().then(notes => {
    res.send(notes)
  })
});

// **************************************************************************************************************************

app.post('/notes', (req, res) => {
  let note = new Note({
    title: req.body.title,
    text: req.body.text
  });

  note.save().then((note) => {
    res.send(note)
  })
})

// **************************************************************************************************************************

app.get('/notes/:id', (req, res) => {
  const id = req.params.id;

  Note.findById(id)
    .then(note => {
      if (!note) {
        res.status(404).send({ message: "There is no note with that id" })
      }
      else {
        res.send(note);
      }
    })
})

// **************************************************************************************************************************
// **************************************************************************************************************************

// **************************************************************************************************************************
  
app.listen(PORT, () => {
  console.log(`Server is listening on port ${PORT}`);
})

