const mongoose = require('mongoose');
const dbConfig = require('./db.config');

mongoose.connect(dbConfig.url, {
  useNewUrlParser: true,
  useUnifiedTopology: true
}).then(() => {
  console.log('Connected to DB')
}).catch((err) => {
  console.log('cannot connect');
  process.exit();
})

module.exports = {
  mongoose
}