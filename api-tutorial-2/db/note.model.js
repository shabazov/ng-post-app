const mongoose = require('mongoose');


const noteSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    minlength: 1,
    trim: true
  }
});

const Note = mongoose.model('Note', noteSchema);
module.exports = { Note }
