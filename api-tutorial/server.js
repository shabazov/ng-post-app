const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const PORT = process.env.PORT || 8080;
const db = require('./app/models')

const app = express();

let corsOptions = {
  origin: 'localhost:8081'
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

db.mongoose
  .connect(db.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false 
  })
  .then(() => { console.log('Connected to database'); })
  .catch((err) => { console.log('Cannot connect to the database', err); process.exit() })



// **********************************************************************************************
// testing route:
app.get('/', (req, res) => {
  res.status(200).json({ message: "Welcome to bezkoder's Tutorial!" })
});

require("./app/routes/tutorial.routes")(app);

// Listen for requests
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
})