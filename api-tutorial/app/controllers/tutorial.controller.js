// create
// findAll
// findOne
// update
// delete
// deleteAll
// findAllPublished

const db = require('../models');
const Tutorial = db.tutorials;

// *****************************************************************************************************************************
// Create and Save a new Tutorial
exports.create = (req, res) => {
  // Validate request
  if (!req.body.title) {
    res.status(400).json({ message: "Content cannot be empty." });
    return;
  }

  // Create the tutorial
  const tutorial = new Tutorial({
    title: req.body.title,
    description: req.body.description,
    published: req.body.published? req.body.published : false
  });

   // Save Tutorial in the database
   tutorial.save(tutorial)
    .then(data => {
      res.send(data)
    })
    .catch(err => {
      res.status(500).send({ message: err.message || "Some error occurred while creating the Tutorial." })
    })
};


// *****************************************************************************************************************************
// Retrieve all Tutorials from the database.
exports.findAll = (req, res) => {
  const title = req.query.title;
  let condition = title? { title: { $regex: new RegExp(title), $options: "i" } } : {};

  Tutorial.find(condition)
    .then(data => { res.send(data) })
    .catch(err => {
      res.status(500).send({ message: err.message || "Some error occurred while retrieving tutorials." })
    })
};

// *****************************************************************************************************************************
// Find a single Tutorial with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Tutorial.findById(id)
    .then(data => {
      if(!data) {
        res.status(404).send({ message: `Tutorial with ID ${id} not found` });
      } else {
        res.send(data)
      }
    })
    .catch(err => {
      res.status(500).send({ message: `Error while retrieving tutorial ${id}` })
    });
};

// *****************************************************************************************************************************
// Update a Tutorial by the id in the request
exports.update = (req, res) => {
  if (!req.body) {
    return res.status(400).send({ message: "Data to update can not be empty!" });
  }

  const id = req.params.id;

  Tutorial.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({ message: `Cannot update Tutorial with id=${id}. Maybe Tutorial was not found!` });
      } else {
        res.send({ message: "Tutorial was updated successfully." });
      }
    })
    .catch(err => {
      res.status(500).send({ message: `Error updating tutorial with ID ${id}` })
    });
};

// *****************************************************************************************************************************
// Delete a Tutorial with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Tutorial.findByIdAndRemove(id)
    .then(data => {
      if (!data) {
        res.status(404).send('Cannot delete the given tutorial')
      } else {
        res.send('Tutorial was deleted successfully')
      }
    })
    .catch(err => {
      res.status(500).send('Could not delete tutorial')
    });
};

// *****************************************************************************************************************************
// Delete all Tutorials from the database.
exports.deleteAll = (req, res) => {
  Tutorial.deleteMany({})
    .then(data => {
      res.send({ message: `${data.deletedCount} tutorials was deleted` });
    })
    .catch(err => {
      res.status(500).send({ message: err.message || "Some error occurred while removing all tutorials." });
    })
};

// *****************************************************************************************************************************
// Find all published Tutorials
// Find all Tutorials with (published = true)
exports.findAllPublished = (req, res) => {
  Tutorial.find({ published: true })
    .then(data => {
      res.send(data)
    })
    .catch(err => {
      res.status(500).send({ message: err.message || "Some error occurred while retrieving tutorials." });
    })
};