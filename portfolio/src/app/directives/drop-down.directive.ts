import { Directive, ElementRef, Renderer2, HostListener } from '@angular/core';

@Directive({
  selector: '[appDropDown]'
})
export class DropDownDirective {

  constructor(private el: ElementRef, private rend: Renderer2) { }

  @HostListener('click') onClick() {
    if (!this.el.nativeElement.classList.contains('open')) {
      this.rend.addClass(this.el.nativeElement, 'open')
    } else {
      this.rend.removeClass(this.el.nativeElement, 'open')
    }
  }
}
