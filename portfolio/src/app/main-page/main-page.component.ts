import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss'],
  animations: [
    trigger('dropped', [
      state('open', style({ height: '100%' })),
      state('close', style({ height: '0%' })),
      transition('open => close', animate('1s ease-out') ),
      transition('close => open', animate('1s ease-out') ),
    ])
  ]
})
export class MainPageComponent implements OnInit {

  isOpen = true;
  dropMenu = 'close'

  constructor() { }

  ngOnInit(): void {}

  showMenu() {
    this.isOpen = !this.isOpen
    this.dropMenu = this.dropMenu === 'close'? 'open' : 'close'; 
  }

}
