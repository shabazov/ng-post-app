import { Component } from '@angular/core';

export interface Post {
  title: string;
  text: string;
  id?: number;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ng-post';
  search = '';

  posts: Post[] = [
    {title: 'Компоненты Angular', text: 'Хочу выучить компоненты Angular, и устроиться веб-разработчиком', id: 1},
    {title: 'Работа на себя', text: 'Хочу открыть свой бизнес', id: 2}
  ]

  updatePosts(post: Post) {
    this.posts.unshift(post)
  }

  deletePost(id: number) {
    this.posts = this.posts.filter(post => post.id !== id)
  }

  searchSet(search: string) {
    this.search = search
  }

}
