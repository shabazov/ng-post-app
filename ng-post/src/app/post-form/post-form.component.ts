import { Post } from './../app.component';
import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';


@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.scss']
})
export class PostFormComponent implements OnInit {

  title = '';
  text = '';
  search = '';

  @Output() onSearch = new EventEmitter<string>()
  @Output() onAdd = new EventEmitter<Post>()
  @ViewChild('inputTitle') titleRef: ElementRef
  @ViewChild('inputText') textRef: ElementRef
  constructor() { }

  ngOnInit(): void {}

  makeSearch() {
    this.onSearch.emit(this.search)
    this.search = ''
  }

  createPost() {
    if (this.title.trim() && this.text.trim()) {
      const post: Post = {
        title: this.title,
        text: this.text,
      }
      console.log('created', post);
      this.onAdd.emit(post);
      this.title = this.text = '';
    }
  }

  focusTitle() {
    this.titleRef.nativeElement.focus()
  }
  focusText() {
    this.textRef.nativeElement.focus()
  }

}
