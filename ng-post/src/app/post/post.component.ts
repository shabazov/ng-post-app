import { Post } from './../app.component';
import { Component, OnInit, Input, Output, EventEmitter, } from '@angular/core';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  @Input() post: Post
  @Output() onDelete = new EventEmitter<number>()

  constructor() { }

  ngOnInit(): void {
  }

  deletePost() {
    this.onDelete.emit(this.post.id)
  }
}
