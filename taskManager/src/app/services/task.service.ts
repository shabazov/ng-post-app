import { Injectable } from '@angular/core';
import { Task } from '../task.interface'

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  tasks: Task[] = [
    {title: 'Marker button issue', task: 'How to handle that f***in button??', id: 2},
    {title: 'Learn Angular', task: 'Best practice', id: 1}
  ]

  updateTasks(task: Task) {
    this.tasks.unshift(task);
  }

  deleteTask(id: number) {
    this.tasks = this.tasks.filter(task => task.id !== id)
  }

  constructor() { }
}
