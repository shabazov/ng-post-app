import { TaskService } from './../services/task.service';
import { Component, OnInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  @ViewChild('taskWrapper') taskRef: ElementRef;

  constructor(public taskService: TaskService, private r: Renderer2) {}

  deleteTask(id: number) {
    this.taskService.deleteTask(id)
  }

  ngOnInit(): void {}

}
