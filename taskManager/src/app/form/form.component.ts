import { TaskService } from './../services/task.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  form: FormGroup

  constructor(private formBuilder: FormBuilder, private taskService: TaskService) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      title: ['', Validators.required],
      task: ['', Validators.required]
    })
  }
  submit() {
    if (this.form.valid) {
      const task = this.form.value;
      task.id = Math.random();
      console.log('task', task);

      this.taskService.updateTasks(task);
      this.form.reset();
    }
  }  
}
