import { Directive, ElementRef, Renderer2, HostBinding, Input, Host, HostListener, ViewChild } from '@angular/core';

@Directive({
  selector: '[appMarker]'
})
export class MarkerDirective {

  constructor(private el: ElementRef, private r: Renderer2) { }

  color = '#32CD32'


  @HostBinding('style.background-color') marked = ''
  
  @HostListener('click') onClick() {
    this.marked = this.color
    this.r.addClass(this.el.nativeElement.parentElement.parentElement, 'marked')
  }

}
