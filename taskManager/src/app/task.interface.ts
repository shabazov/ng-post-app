export interface Task {
  title: string;
  task: string;
  id?: number;
}