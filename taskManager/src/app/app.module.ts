import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { AppComponent } from './app.component';
import { FormComponent } from './form/form.component';
import { PostComponent } from './post/post.component';
import { MarkerDirective } from './directives/marker.directive';


@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    PostComponent,
    MarkerDirective,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
