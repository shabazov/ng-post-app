import { Component } from '@angular/core';
import { trigger, state, style, transition, animate, group, query, keyframes } from '@angular/animations';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('box', [
      state('start', style( { background: 'teal' } )),
      state('end', style( { background: 'white', transform: 'scale(1.1)', color: '#000' } )),
      state('special', style( { background: 'orange', transform: 'rotate(720deg)', borderRadius: '50%' } )),
      transition('end <=> start', animate('800ms ease-in')),
      transition('* <=> special', [
        group([
          query('h3', animate(1000, style({ fontSize: '36px' }))),
          style({ transform: 'rotate(-720deg)' }), animate('.6s ease-in'),
          style({ borderRadius: '50%' }), animate('.3s ease-in'),
        ])
      ]),
      // void => *
      transition(':enter', [
        animate('3s', keyframes([
          style({ background: 'white', offset: 0 }),
          style({ background: 'yellow', offset: 0.2 }),
          style({ background: 'green', offset: 0.4 }),
          style({ background: 'teal', offset: 1 }),
        ]))
        // style({ opacity: 0 }),
        // animate('.8s ease-out')
      ]),
      // * => void
      transition(':leave', [
        style({ opacity: 1, transform: 'scale(1.2)' }),
        group([
          animate(1000, style({ opacity: 0, transform: 'scale(0.2)' })),
          animate(700, style({ color: 'red' }))
        ])
      ])
    ]),
  ]
})
export class AppComponent {
  boxState = 'start';
  visible = true;

  animate() {
    this.boxState = this.boxState === 'start' ? 'end' : 'start';
  }

  animationStarted(event: AnimationEvent) {
    console.log('aniimated', event);
    
  }
}
