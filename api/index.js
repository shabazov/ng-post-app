const Joi = require('@hapi/joi');
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

app.use(express.json());

let users = [
  {id: 1, name: "Tania", country: "Spain", loves: [
    "her doughter Letty", "me", "white wine"
  ]},
  {id: 2, name: "Egor", country: "Spain", loves: [
    "Tania", "Dasha", "red wine"
  ]}
];

app.get('/', (req, res) => {
  res.send(users)
});

app.get('/api/users/:id', (req, res) => {
  const user = users.find( user => user.id === parseInt(req.params.id) );
  if (!user) res.status(404).send('There is no user with that id'); // 404 NOT FOUND  
  else { res.send(user) }
});

//// *** POST ***

app.post('/api/users', (req, res) => {
  
  const { error } = validateUser(req.body)

  // 400 bad request
  if (error) return res.status(400).send(error);
  

  const user = {
    id: users.length + 1,
    name: req.body.name,
    country: req.body.country
  };
  users.push(user);
  res.send(user);
});

app.put('/api/users/:id', (req, res) => {
  // find specified user
  let user = users.find( user => user.id === parseInt(req.params.id) )
  if (!user) return res.status(404).send('There is no user with that id'); // 404 NOT FOUND

  // Validate
  // If invalid --> Bad request 400
  const { error } = validateUser(req.body)

  if (error) {
    // 400 bad request
    res.status(400).send(error);
    return;
  };

  // Update user
  user.name = req.body.name;
  user.country = req.body.country;
  user.loves = req.body.loves;
  res.send(user)
});


app.delete('/api/users/:id', (req, res) => {
  // FIND
  let user = users.find( user => user.id === parseInt(req.params.id) )
  if (!user) return res.status(404).send('There is no user with that id'); // 404 NOT FOUND

  // Delete
  const index = users.indexOf(user);
  users.splice(index, 1);

  // Send back to client
  res.send(user);
})



app.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
});


function validateUser(user) {
  const schema = Joi.object({
    name: Joi.string().min(3).required(),
    country: Joi.string().min(3).required()
  });

  return schema.validate(user);
}