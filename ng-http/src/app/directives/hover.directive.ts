import { Directive, ElementRef, HostListener, HostBinding, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appHover]'
})
export class HoverDirective {

  possibleColors = [
    'darksalmon', 'hotpink', 'lightskyblue', 'goldenrod', 'peachpuff',
    'mediumspringgreen', 'cornflowerblue', 'blanchedalmond', 'lightslategrey'
  ];



  constructor(private el: ElementRef, private r: Renderer2) {}

  @HostBinding('style.background') color = null;
  @Input('appHover') value = '';
  @HostListener('click') onClick() {
    this.r.setStyle(this.el.nativeElement, 'background', this.value)
  }

  @HostListener('mouseover') backHover() {
    const colorPick = Math.floor(Math.random() * this.possibleColors.length);
    this.r.setStyle(this.el.nativeElement, 'background', this.possibleColors[colorPick])
    // this.el.nativeElement.style.background = this.possibleColors[colorPick];
    // this.el.nativeElement.style.background = 'black'
  }

  @HostListener('mouseleave') dropHover() {
    this.r.setStyle(this.el.nativeElement, 'background', null)
  }

}
