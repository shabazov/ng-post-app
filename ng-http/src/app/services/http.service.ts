import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { delay, catchError } from 'rxjs/operators'
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }

  getImages(): Observable<any> {
    let params = new HttpParams();
    params = params.append('_limit', '20');

    const headers = new HttpHeaders({
      myHeader: 'custom_header'
    })
    return this.http.get<any>('http://jsonplaceholder.typicode.com/photos', {
      params,
      headers
    })
    .pipe(
      delay(1500),
      catchError(error => {
        console.log('error', error.message);
        return throwError(error)
      })
    )
  }
}
