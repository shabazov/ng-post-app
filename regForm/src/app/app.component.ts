import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  form: FormGroup;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.form = this.formBuilder.group({
     firstName: ['', [Validators.required]],
     lastName: ['', [Validators.required]],
     email: ['', [Validators.required, Validators.email]],
     password: [null, [Validators.required, Validators.minLength(6)]],
     confirmPassword: [null, [Validators.required, Validators.minLength(6)]],
     phoneNumber: [null, [Validators.required, Validators.minLength(9), Validators.pattern("^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$")]],
     gender: [''],
     address: [''],
     zipCode: [null],
     conditions: [null]
    }, {
      validator: this.confirmValidator('password', 'confirmPassword')
    })
  };

  confirmValidator(password: string, confirm: string) {
    return (formGroup: FormGroup) => {
      const controlPassword = formGroup.controls[password];
      const controlConfirm = formGroup.controls[confirm];

      if(controlPassword.errors) {
        return
      }

      if(controlPassword.value !== controlConfirm.value) {
        controlConfirm.setErrors( { confirmValidator: true } )
      } else {
        controlConfirm.setErrors(null)
      }
    }
  }

  submit() {
    if (this.form.valid) {
      const formData = this.form.value;
      console.log('form data:', formData);
      alert('Вы успешно зарегестрировались');
    }
  }
}
