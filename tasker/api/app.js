require('dotenv').config()

const express = require('express');
const app = express();
const port = process.env.DATABASE_URL || 3000;
const mongoose = require('mongoose');

// Connect to mongoDB
mongoose.connect(port, { useUnifiedTopology: true, useNewUrlParser: true });

const db = mongoose.connection;
db.on('error', (error) => console.error(error));
db.once('open', () => console.log('Connected to DB'));



app.use(express.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Methods", "GET, POST, HEAD, OPTIONS, PUT, PATCH, DELETE");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

const listsRouter = require('./routes/lists');
app.use('/lists', listsRouter)

app.listen(3000, () => {
  console.log(`Server is listening on port 3000`);
});