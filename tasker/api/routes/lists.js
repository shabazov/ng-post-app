const express = require('express');
const router = express.Router();
const List = require('../models/list.model');

// get all
router.get('/', async (req, res) => {
  try {
    const lists = await List.find();
    res.json(lists)
  } catch (error) {
    res.status(500).json( { message: error.message } )
  }
});

//get one
router.get('/:id', getList, (req, res) => {
  res.json(res.list)
});

// create one
router.post('/', async (req, res) => {
  const list = new List({
    title: req.body.title
  });
  try {
    const newList = await list.save();
    res.status(201).json(newList)
  } catch (error) {
    res.status(400).json({ message: error.message })
  }
});

// update one
router.patch('/:id', getList, async (req, res) => {
  if (req.body.title !== null) {
    res.list.title = req.body.title;
  }
  try {
    const updated = await res.list.save();
    res.status(201).json(updated)
  } catch (error) {
    res.status(400).json({ message: error.message })
  }
  
})

// delete one
router.delete('/:id', getList, async (req, res) => {
  try {
    await res.list.remove();
    res.json({ message: 'List successfully removed.' })
  } catch (error) {
    res.status(500).json({ message: error.message })
  }
})

async function getList(req, res, next) {
  let list
  try {
    list = await List.findById(req.params.id);
    if(list === null) {
      return res.status(404).json({ message: "cannot find the list" })
    }
  } catch (err) {
    return res.status(500).json({ message: err.message })
  }
  res.list = list;
  next()
}

module.exports = router;