import { List } from './../list.interface';
import { Router } from '@angular/router';
import { TaskService } from './../task.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-list',
  templateUrl: './new-list.component.html',
  styleUrls: ['./new-list.component.scss']
})
export class NewListComponent implements OnInit {

  constructor(private taskService: TaskService, private router: Router) { }

  ngOnInit(): void {}

  addList(title: string) {
    this.taskService.addList(title)
      .subscribe((list: List) => {})
    this.router.navigate(['/'])
  }
}
