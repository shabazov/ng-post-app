import { List } from './../list.interface';
import { Component, OnInit } from '@angular/core';
import { TaskService } from '../task.service';
import { ActivatedRoute, Params, Router } from '@angular/router'; 

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {

  lists: List[];

  selectedListId: string;

  constructor(private taskService: TaskService, private route: ActivatedRoute) {}

  ngOnInit() {
    // this.route.params.subscribe(
    //   (params: Params) => {
    //     console.log(params);
        
    //     this.selectedListId = params.listId;
    //     console.log(this.selectedListId);
        
    //   }
    // )

    this.taskService.getLists()
      .subscribe(lists => {
        this.lists = lists;
        console.log( 'list' ,this.lists);     
      })
  };

  deleteList(list: List) {
    
    this.taskService.deleteList()
      .subscribe(() => {
        console.log(`list with successfully deleted`);
      })
  }
  
}
