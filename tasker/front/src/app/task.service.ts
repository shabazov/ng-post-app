import { List } from './list.interface';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TaskService {


  constructor(private http: HttpClient) { }

  getLists(): Observable<List[]> {
    return this.http.get<List[]>('http://localhost:3000/lists');
  }

  addList(title: string) {
    return this.http.post('http://localhost:3000/lists', {title});
  }

  deleteList() {
    return this.http.delete('http://localhost:3000/lists')
  }
}
