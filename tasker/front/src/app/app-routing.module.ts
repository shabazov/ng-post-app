import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TasksComponent } from './tasks/tasks.component';
import { NewListComponent } from './new-list/new-list.component';


const routes: Routes = [
  { path: '', component: TasksComponent },
  { path: 'new-list', component: NewListComponent },
  { path: 'lists/:listId', component: TasksComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
