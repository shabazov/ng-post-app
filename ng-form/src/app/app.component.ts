import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  facebook: string = 'https://i.pinimg.com/originals/ef/b5/d9/efb5d90eeb07bb5914f094007da4c2f7.png'
  twitter: string = 'https://www.ingresosviaweb.com/wp-content/uploads/2017/01/twitterlogo.png'
  instagram: string = 'https://i.pinimg.com/originals/b5/28/ca/b528caf79d37050cf377aec16143805c.png'
  whatsapp: string = 'https://www.shareicon.net/data/256x256/2017/06/21/887423_text_512x512.png'

  toggler() {
    document.querySelector('.cont').classList.toggle('s-signup')
  }

  formIn: FormGroup
  formUp: FormGroup

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.formIn = new FormGroup({
      email: new FormControl('', [Validators.email, Validators.required]),
      password: new FormControl(null, [Validators.required, Validators.minLength(6)])
    })

    this.formUp = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.email, Validators.required]],
      password: [null, [Validators.required, Validators.minLength(6)]],
      confirmPassword: [null, [Validators.required, Validators.minLength(6)]]
    }, {
      validator: this.mustMatch('password', 'confirmPassword')
    })
  }

  mustMatch(controlName: string, matchingControlName: string) {

    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true })
      } else {
        matchingControl.setErrors(null)
      }
    }
  }


  submitUp() {
    if (this.formUp.valid) {
      const signedUpData = this.formUp.value
      console.log(signedUpData);
      alert('Вы успешно зарегестрировались!')
    }
  }


  submitIn() {
    if (this.formIn.valid) {
      const logInData = this.formIn.value
      console.log('Logged in data :', logInData);
      alert('Вход выполнен успешно!')
    }
  }
}
