import { RefDirective } from './ref.directive';
import { ModalComponent } from './modal/modal.component';
import {Component, ComponentFactoryResolver, ViewChild} from '@angular/core'
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  @ViewChild(RefDirective) refDir: RefDirective
  
  constructor(private resolver: ComponentFactoryResolver, private title: Title, private meta: Meta) {
    title.setTitle('SEO tags');
    meta.addTags([
      {name: 'keywords', content: 'angular, google, tags'},
      {name: 'description', content: 'this is SEO tags AppComponent'}
    ])  
  };

  

  showModal() {
    const modalFactory = this.resolver.resolveComponentFactory(ModalComponent)
    this.refDir.containerRef.clear();

    const component = this.refDir.containerRef.createComponent(modalFactory);

    component.instance.title = 'Dynamic components';
    component.instance.close.subscribe(() => {
      this.refDir.containerRef.clear()
    })
  }
}

